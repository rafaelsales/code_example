class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.boolean :active
      t.belongs_to :offer

      t.timestamps
    end
    add_index :coupons, :offer_id
  end
end
