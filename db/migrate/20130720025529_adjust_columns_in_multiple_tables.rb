class AdjustColumnsInMultipleTables < ActiveRecord::Migration
  def change
    # Users
    add_column :users, :birthdate, :date
    rename_column :users, :uid, :facebook_uid
    rename_column :users, :authentication_token, :facebook_oauth_token
    remove_column :users, :provider
    change_column :users, :used_coupons_count, :integer, default: 0
    change_column :users, :all_coupons_count, :integer, default: 0
    add_index :users, :name
    add_index :users, :last_sign_in_at

    # Stores
    add_index :stores, :name

    # Offer
    remove_column :coupons, :active

    # Badges
    add_index :badges, :title
  end
end
