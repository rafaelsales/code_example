class UserDashboardController < ApplicationController
  include UserDashboardControllersSharedBehavior

  def promr_box
    @offer_shares = current_user.offer_shares.limit(5)
  end

  def friends
    @friends = current_user.friends.preview_info.order_by_name
  end

  def search
    @search = params[:search]
    if @search.blank?
      errors << 'Digite pelo menos uma palavra a ser pesquisada'
      redirect_to root_path
    else
      @all_users = User.search(@search)
      @all_stores = Store.search(@search)
      @all_offers = Offer.search(@search)
    end
  end

end
