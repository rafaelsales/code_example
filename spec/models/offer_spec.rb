require 'spec_helper'

describe Offer do

  it { should have_many :coupons }
  it { should belong_to :store }

  describe 'validations' do
    [:title, :description, :start_time, :end_time, :coupons_provided, :discount].each do |attr|
      it { should validate_presence_of attr }
    end

    it { should validate_numericality_of(:coupons_provided).only_integer }

    describe 'start_time and end_time' do
      (Offer::Status::ALL - [Offer::Status::ARCHIVED]).each do |status|
        context "when offer is #{status}" do
          it 'should add error when start_time is before end_time' do
            offer = build :offer, start_time: Time.current, end_time: 1.day.ago, status: status
            offer.valid?
            expected_message = I18n.t('offer.validation.end_time')
            expect(offer.errors[:end_time]).to include expected_message
          end

          it 'should add error when duration limit is exceeded' do
            offer = build :offer, start_time: 5.days.ago,
                          end_time: Offer::DURATION_LIMIT.days.from_now
            offer.valid?
            expected_message = I18n.t('offer.validation.duration_limit', limit: Offer::DURATION_LIMIT)
            expect(offer.errors[:end_time]).to include expected_message
          end
        end
      end

      context 'when offer is archived' do
        it 'should not validate time attributes' do
         offer = build :archived_offer
         offer.end_time = offer.start_time + 1.day
         offer.valid?
         expect(offer.errors.keys.include? :end_time).to be false
        end
      end
    end
  end

  describe 'before_validation' do
    it 'calculates the discount when prices are defined' do
      offer = build :offer, prices_defined: true, original_price: 200, current_price: 50
      offer.assign_attributes discount: nil
      expect(offer.discount).to be nil

      offer.valid?
      expect(offer.discount).to be 75
    end

    it 'clean out price values when user decides to define only a discount value' do
      offer = build :offer, prices_defined: true
      expect(offer.original_price).to_not be nil
      expect(offer.current_price).to_not be nil

      offer.prices_defined = false
      offer.valid?
      expect(offer.original_price).to be nil
      expect(offer.current_price).to be nil
    end
  end

  describe 'before_destroy' do
    it 'allows destroy when offer is deactivated' do
      offer = create :offer, status: Offer::Status::DEACTIVATED
      expect(offer.destroy).to be offer
    end

    (Offer::Status::ALL - [Offer::Status::DEACTIVATED]).each do |status|
      it "does not allow destroy when offer is #{status}" do
        offer = create :offer, status: status
        expect(offer.destroy).to be false
      end
    end
  end

  describe 'can_delete?' do
    it 'is true if offer is deactivated' do
      offer = build :offer, status: Offer::Status::DEACTIVATED
      expect(offer.can_delete?).to be true
    end

    (Offer::Status::ALL - [Offer::Status::DEACTIVATED]).each do |status|
      it "is false if offer is #{status}" do
        offer = build :offer, status: status
        expect(offer.can_delete?).to be false
      end
    end
  end

  describe 'can_edit?' do
    it 'is true if offer if activate transition is allowed' do
      offer = build :offer, status: Offer::Status::DEACTIVATED
      expect(offer.can_activate?).to be true
      expect(offer.can_edit?).to be true
    end

    it "is false if offer activate transition is not allowed" do
      offer = build :archived_offer
      expect(offer.can_activate?).to be false
      expect(offer.can_edit?).to be false
    end
  end

  describe 'finished?' do
    it 'is true if offer is archived' do
      expect(build(:archived_offer).finished?).to be true
    end

    it 'is true if offer end_time has met' do
      offer = build :offer, end_time: 1.hour.ago
      expect(offer.finished?).to be true
    end

    it 'is true if there are no coupons available' do
      offer = build :offer, coupons_provided: 1, coupons: [build(:coupon)]
      expect(offer.finished?).to be true
    end

    (Offer::Status::ALL - [Offer::Status::ARCHIVED]).each do |status|
      it "is false if offer is #{status}, end_time has not met and there are coupons available" do
      offer = build :offer, status: status, coupons_provided: 2, coupons: [build(:coupon)]
      expect(offer.finished?).to be false
      end
    end
  end

  describe 'generate_coupon' do
    it 'generates a coupon associated with given user' do
      user = create :user
      create(:offer).generate_coupon(user)

      expect(user.coupons.count).to be 1
    end
  end

  describe 'on_going?' do
    it 'is true if offer has started and has not finished' do
      expect(build(:offer).on_going?).to be true
    end

    it 'is false if offer has not started' do
      offer = build :offer, start_time: 1.day.from_now
      expect(offer.on_going?).to be false
    end

    it 'is false if offer has finished' do
      offer = build :offer, end_time: 1.day.ago
      expect(offer.on_going?).to be false
    end
  end

  describe 'started?' do
    it 'is true if offer is activated and start_time has met' do
      expect(build(:offer).started?).to be true
    end

    (Offer::Status::ALL - [Offer::Status::ACTIVATED]).each do |status|
      it "is false if offer is #{status}" do
        offer = build :offer, status: status
        expect(offer.started?).to be false
      end
    end

    it 'is false if offer is activated but start_time has not met' do
      offer = build :offer, start_time: 1.hour.from_now
      expect(offer.started?).to be false
    end
  end

  describe 'share' do
    it 'creates an OfferShare object associated with offer and proper users' do
      from_user = create :user
      to_user = create :user
      offer = create :offer
      offer_share = offer.share from: from_user, to: to_user

      expect(offer_share.from_user).to be from_user
      expect(offer_share.to_user).to be to_user
      expect(offer_share.offer).to be offer
    end
  end

  describe 'state machine' do
    it 'is deactivated when new offer is initialized' do
      offer = Offer.new
      expect(offer.deactivated?).to be true
    end

    describe 'transition from any state to archived' do
      Offer::Status::ALL.each do |status|
        it "transitions from #{status} to archived" do
          offer = build :offer, status: status
          offer.archive
          expect(offer.archived?).to be true
        end
      end
    end

    describe 'transition from deactivated to activated' do
      it 'is allowed if offer has not finished' do
        offer = build :offer, status: Offer::Status::DEACTIVATED
        offer.activate
        expect(offer.activated?).to be true
      end

      it 'is not allowed if offer has finished' do
        offer = build :offer, status: Offer::Status::DEACTIVATED, end_time: 1.hour.ago
        offer.activate
        expect(offer.activated?).to be false
      end
    end

    describe 'transition from activated to deactivated' do
      it 'is allowed if offer has no coupons and has not finished' do
        offer = build :offer
        offer.deactivate
        expect(offer.deactivated?).to be true
      end

      it 'is not allowed if offer has coupons' do
        offer = build :offer, coupons: [build(:coupon)]
        offer.deactivate
        expect(offer.deactivated?).to be false
      end

      it 'is not allowed if offer has finished' do
        offer = build :offer, end_time: 1.hour.ago
        offer.deactivate
        expect(offer.deactivated?).to be false
      end
    end
  end
end
