PromrWeb::Application.routes.draw do

  #########################
  # Routes for Users only #
  #########################

  devise_scope :users do
    get '/users/sign_in', to: redirect('/'), :as => :new_user_session
  end

  devise_for :users,
             controllers: { :omniauth_callbacks => "users/omniauth_callbacks" },
             skip: [:registrations, :passwords, :unlocks]

  controller :home do
    get '/home', action: :index
    get '/contact_us', action: :contact_us, as: :contact_us
    get '/terms', action: :terms, as: :terms
  end

  # Landing page, for non-logged user
  unauthenticated :user do
    root to: redirect('/home')
  end

  # User dashboard, when logged
  authenticated :user do
    root controller: :user_dashboard, action: :promr_box
  end

  controller :user_dashboard do
    get '/search', action: :search, as: :search
    get '/friends', action: :friends, as: :friends
    get '/facebook_friends', action: :facebook_friends, as: :facebook_friends
  end

  controller :user_profile do
    get '/me/edit', action: :edit, as: :edit_user_profile
    put '/me', action: :update, as: :update_user_profile
    get '/me', action: :show_me, as: :my_profile

    get '/u/:id', action: :show, as: :user_profile

    get '/u/:id/invite_friend', action: :invite_friend, as: :invite_friend
    get '/u/:id/approve_friend', action: :approve_friend, as: :approve_friend
    get '/u/:id/cancel_invite', action: :cancel_invite, as: :cancel_invite
    get '/u/:id/remove_friend', action: :remove_friend, as: :remove_friend
    get '/u/:id/block_friend', action: :block_friend, as: :block_friend
    get '/u/:id/unblock_friend', action: :unblock_friend, as: :unblock_friend
  end

  resources :coupons, only: [:index, :show]

  resources :stores, only: [:index]

  resources :offers, only: [:show] do
    put 'new_coupon', action: :new_coupon, as: :new_coupon
  end

  ###############################
  # Routes for Store Users only #
  ###############################
  devise_for :store_users

  get '/store_dashboard', action: :index, controller: :store_dashboard, as: :store_user_root

  namespace :store_dashboard do
    post '/change_image', action: :change_image
    resources :offers do
      get '/activate', action: :activate
      get '/deactivate', action: :deactivate
    end
    resources :coupons do
      get '/buy', action: :buy, :on => :collection
    end

    post '/use_coupon', action: :use_coupon
    get '/use_coupon' => redirect('/store_dashboard/')
    get '/profile', action: :profile
    get '/help', action: :profile
  end

  ###############################
  # Routes for Admin Users only #
  ###############################
  devise_for :admin_users

  get '/admin_dashboard', action: :index, controller: :admin_dashboard, as: :admin_user_root

  namespace :admin_dashboard do
    resources :stores
    resources :offers
    resources :store_users
  end

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end


  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
