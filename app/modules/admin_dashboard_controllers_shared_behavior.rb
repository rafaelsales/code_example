module AdminDashboardControllersSharedBehavior
  extend ActiveSupport::Concern

  included do
    before_filter :authenticate_admin_user!

    layout 'admin_dashboard'
  end
end
