class AdminUser < ActiveRecord::Base
  devise :database_authenticatable,
        :rememberable, :trackable, :validatable

  attr_accessible :name, :email, :password, :password_confirmation, :remember_me

  belongs_to :store
end
