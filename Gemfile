source 'https://rubygems.org'

gem 'rails', '3.2.13'
gem 'rails-i18n'

gem 'acts_as_follower'
gem 'amistad' #Add friendship support
gem 'brazilian-rails' #Validates CNPJ
gem 'carrierwave' #Gem like PaperClip
gem 'certified' #SSL
gem 'devise' #Authorization
gem 'koala' #Client for post in Facebook
gem 'mini_magick' #A ruby wrapper for ImageMagick or GraphicsMagick command line
gem 'mysql2'
gem 'omniauth' #General Auth
gem 'omniauth-facebook'
gem 'state_machine'
gem 'time_diff'
gem 'timecop'

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  # gem 'therubyracer', :platforms => :ruby
  gem 'therubyracer' #Required by Rails when deploying
  gem 'execjs' #Required by Rails when deploying

  gem 'sass-rails'
  gem 'coffee-rails'
  gem 'jquery-rails'
  gem 'twitter-bootstrap-rails'
  gem 'bootstrap-datetimepicker-rails'
  gem 'uglifier'
end

#Gems below temporarily available in production
gem 'factory_girl_rails' # Factory for generating model objects more comfortably
gem 'faker' # Generates random data

group :development, :test do
  gem 'meta_request' #Meta Request to work with Rails Panel in Google Chrome
  gem 'better_errors' #Better Errors view
  gem 'binding_of_caller' # local/instance variable inspection, pretty stack frame names) in Better Errors
  gem 'pry-rails' # Better Rails Console and allows inject breakpoints in code with binding.pry
  gem 'proxylocal'
  gem 'rspec-rails' # A more comprehensive Ruby+Rails testing framework
end

group :test do
  gem 'shoulda-matchers' # Convenient assertions, mainly for Rails
  gem 'mocha' # Allows mock, stub and assertions
end

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the app server
# gem 'unicorn'

# Deploy with Capistrano
# gem 'capistrano'

# To use debugger
# gem 'debugger'
