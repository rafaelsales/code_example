class CreateStores < ActiveRecord::Migration
  def change
    I18n.locale = PromrWeb::Application.config.i18n.default_locale
    create_table :stores do |t|
      t.string :name
      t.string :address
      t.string :phone
      t.string :contact
      t.string :name_preposition, default: I18n.t('store.default_name_preposition')

      t.timestamps
    end
  end
end
