# encoding: UTF-8
require 'factory_girl'
include FactoryGirl::Syntax::Methods

# Admin users
puts "Creating Admin Users"
create(:admin_user, email: 'admin@promr.com.br')

# Malls
puts "Creating Malls"
mall_iguatemi = create(:mall, name: 'Iguatemi')

# Stores
puts "Creating Stores"
store_subway = create(:store, name: "Subway", mall: mall_iguatemi, phone: "(85) 3241-1868", contact: "José Silva", image_name: 'subway.jpg',
                      address: "Av. Washington Soares, 85\nShopping Iguatemi\nBairro Edson Queiroz\nFortaleza - CE\n60811-340")
store_skyler = create(:store, name: "Skyler", mall: mall_iguatemi, phone: "(85) 3241-3576", contact: "João de Santo Cristo", image_name: 'skyler.jpg',
                      address: "Av. Washington Soares, 85\nShopping Iguatemi\nBairro Edson Queiroz\nFortaleza - CE\n60811-340")
store_sushiloko = create(:store, name: "Sushiloko", mall: mall_iguatemi, phone: "(85) 3114-0202", contact: "Victor Bedê", image_name: 'sushiloko.jpg',
                         address: "Av. Washington Soares, 909\nShopping Salinas\nBairro Edson Queiroz\nFortaleza - CE\n60820-900")

# Store Users
puts "Creating Store Users"
create(:store_user, email: 'admin@subway.com.br', store: store_subway)
create(:store_user, email: 'admin@skyler.com.br', store: store_skyler)
create(:store_user, email: 'admin@sushiloko.com.br', store: store_sushiloko)

# Offers
puts "Creating Offers"
create(:offer, store: store_subway, title: "1 Sanduíche Club de 15 cm + 1 copo de Refri 300ml")
create(:offer, store: store_skyler, title: "Qualquer camisa polo com desconto")
create(:offer, store: store_sushiloko, title: "Combinado com 6 sushis, 4 sashimis, 8 hots e 1 temaki")

# Users
puts "Creating Users"
user_franze = create(:user, email: 'franzejr@gmail.com', name: 'Franzé Junior', image_name: '1.jpg',
                     facebook_uid: '100001731897155', coupons: [create(:coupon, offer: store_subway.offers.first)])
user_pedro = create(:user, email: 'pisandelli@gmail.com', name: 'Pedro Pisandelli', image_name: '2.jpg',
                    facebook_uid: '100000325641623', coupons: [create(:coupon, offer: store_sushiloko.offers.first)])
user_rafael = create(:user, email: 'rafaelcds@gmail.com', name: 'Rafael Sales', image_name: '3.jpg',
                     facebook_uid: '1201306089', coupons: [create(:coupon, offer: store_skyler.offers.first)])

user_franze.invite(user_pedro); user_pedro.approve(user_franze)
user_pedro.invite(user_rafael); user_rafael.approve(user_pedro)

User.all.each do |user|
  user.follow(store_subway); user.follow(store_skyler)
end

# Offer Shares
puts "Creating Offer Shares"
Offer.all.each do |offer|
  create(:offer_share, from_user: user_franze, to_user: user_pedro, offer: offer)
  create(:offer_share, from_user: user_pedro, to_user: user_rafael, offer: offer)
  create(:offer_share, from_user: user_rafael, to_user: user_franze, offer: offer)
end

# Badges
puts "Creating Badges"
7.times { |n| create(:badge, image_name: "#{1 + n}.png" ) }
User.all.each do |user|
  user.badges << Badge.find(1, 2, 3)
end
