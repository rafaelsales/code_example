class OfferShare < ActiveRecord::Base
  attr_accessible :unread, :from_user, :to_user, :offer

  belongs_to :from_user, class_name: 'User', foreign_key: :from_user_id
  belongs_to :to_user, class_name: 'User', foreign_key: :to_user_id
  belongs_to :offer

  validates :from_user, :to_user, :offer, presence: true

  class << self
    def find_by_offer_and_receiver offer, user
      where(offer_id: offer.id, to_user_id: user.id)
    end
  end
end
