class AddColumnsToCoupons < ActiveRecord::Migration
  def change
    add_column :coupons, :published_at, :timestamp
    add_column :coupons, :used_at, :timestamp
    add_column :coupons, :code, :string
    add_index :coupons, :code, unique: true
  end
end
