class RemoveTypeFromAdminUser < ActiveRecord::Migration
  def change
    remove_column :admin_users, :type
  end
end
