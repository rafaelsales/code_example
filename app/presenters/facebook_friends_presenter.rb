class FacebookFriendsPresenter
  attr_accessor :user

  def initialize user
    self.user = user
  end

  def friends
    @friends ||= user.facebook_proxy.friends.tap do |friends|
      users_mapped_by_facebook_uids = User.map_by_facebook_uids(friends)

      friends.each do |fb_friend|
        fb_friend.promr_user = users_mapped_by_facebook_uids[fb_friend.uid.to_s]
      end
    end
  end

  def friends_not_signed_up
    friends.select { |friend| friend.promr_user.blank? }
  end

  def friends_signed_up
    friends.select { |friend| friend.promr_user.present? }
  end
end
