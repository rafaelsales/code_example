require 'spec_helper'

describe User do
  [:coupons, :unread_offer_shares, :offer_shares, :user_badges].each do |association|
    it { should have_many association }
  end
  it { should have_many(:badges).through(:user_badges) }

  describe 'unread_offer_shares' do
    it 'retrieves only the unread offer shares' do
      user = create :user
      expected_share = create(:offer_share, to_user: user, unread: true)
      create(:offer_share, to_user: user, unread: false)

      expect(user.unread_offer_shares).to eq [expected_share]
    end

    it 'retrieves ordered by last created' do
      user = create :user
      first = create(:offer_share, to_user: user, created_at: 4.days.ago)
      second = create(:offer_share, to_user: user, created_at: 2.days.ago)
      expect(user.unread_offer_shares).to eq [second, first]
    end
  end

  context 'friendship' do
    before(:each) do
      @user_1 = create :user
      @user_2 = create :user
    end

    describe 'allow_invite?' do
      it 'is true if given user is not friend, was not invited and not blocked' do
        expect(@user_1.allow_invite? @user_2).to be true
      end

      it 'is false if users are friends' do
        @user_1.invite(@user_2)
        @user_2.approve(@user_1)

        expect(@user_1.allow_invite? @user_2).to be false
      end

      it 'is false if users are friends of have blocked each other' do
        @user_1.stubs(:friend_with_or_blocked?).returns(true)

        expect(@user_1.allow_invite? @user_2).to be false
      end
    end

    describe 'allow_cancel_invite?' do
      before(:each) do
        @user_1 = create :user
        @user_2 = create :user
      end

      it 'is true if given user was invited, was not invited and not blocked' do
        expect(@user_1.allow_invite? @user_2).to be true
      end

      it 'is false if there is already an invitation' do
        @user_1.invite(@user_2)

        expect(@user_1.allow_invite? @user_2).to be false
      end

      it 'is false if users are friends of have blocked each other' do
        @user_1.stubs(:friend_with_or_blocked?).returns(true)

        expect(@user_1.allow_invite? @user_2).to be false
      end
    end

    describe 'friend_with_or_blocked?' do
      it 'is true if users are friends' do
        @user_1.invite(@user_2)
        @user_2.approve(@user_1)

        expect(@user_1.friend_with_or_blocked? @user_2).to be true
      end

      it 'is true if a user has blocked the other' do
        @user_1.invite(@user_2)
        @user_2.approve(@user_1)
        @user_1.block(@user_2)

        expect(@user_1.friend_with_or_blocked? @user_2).to be true
      end

      it 'is false if users are not friends and did not have blocked each other' do
        expect(@user_1.friend_with_or_blocked? @user_2).to be false
      end
    end
  end

  describe 'should_review_profile?' do
    it 'is true if user has never signed in' do
      expect(build(:user).should_review_profile?).to be true
    end

    it 'is false if user has already signed in' do
      user = build(:user, sign_in_count: 10)
      expect(user.should_review_profile?).to be false
    end
  end
end
