class CreateOfferSharesTable < ActiveRecord::Migration
  def change
    create_table :offer_shares do |t|
      t.integer :from_user_id, null: false
      t.integer :to_user_id, null: false
      t.integer :offer_id, null: false
      t.boolean :unread, null: false, default: false

      t.timestamps
    end

    add_index :offer_shares, [:to_user_id, :unread] # to retrieve a user's promr inbox
    add_index :offer_shares, [:to_user_id, :offer_id] # to retrieve a particular offer share
  end
end
