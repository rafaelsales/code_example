class AddCnpjToStores < ActiveRecord::Migration
  def change
    change_table :stores do |t| 
      t.string :cnpj
    end
  end
end
