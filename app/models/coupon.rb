class Coupon < ActiveRecord::Base
  attr_accessible :code, :user, :offer, :used_at

  belongs_to :offer, :counter_cache => :coupons_count
  belongs_to :user, :counter_cache => :all_coupons_count

  before_create do |coupon|
    coupon.code = generate_unique_code
  end

  def can_be_used?
    !used? && offer.on_going?
  end

  def use!
    errors.add :base, I18n.t('coupon.already_used', :code => code, used_at: used_at) if used?
    errors.add :base, I18n.t('coupon.not_active', :code => code) unless offer.on_going?
    return false if errors.present?

    Coupon.transaction do
      self.user.used_coupons_count += 1
      self.user.save!

      self.update_attributes! used_at: Time.now
    end
    true
  end

  def used?
    used_at.present?
  end

  class << self
    def find_by_id_and_user_id id, logged_user
      where(id: id, user_id: logged_user.id).first
    end

    def find_by_code_and_store code, store
      includes(:offer).where(code: code.try(:upcase), offers: { store_id: store.id } ).first
    end
  end

  private

  def generate_unique_code
    begin
      temp_code = generate_code
    end while Coupon.where('code = ? AND used_at IS NULL', temp_code).exists?
    temp_code
  end

  def generate_code
    temp_code = ''
    3.times { temp_code << ('0A'..'9Z').to_a.sample }
    temp_code
  end
end
