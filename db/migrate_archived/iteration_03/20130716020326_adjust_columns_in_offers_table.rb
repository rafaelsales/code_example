class AdjustColumnsInOffersTable < ActiveRecord::Migration
  def change
    remove_column :offers, :coupon_expires_on
  end
end
