# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#Admin Users
AdminUser.create(email: 'franzejr@gmail.com', password: 'franzejr123')
AdminUser.create(email: 'rafaelcds@gmail.com', password: 'rafaelcds123')
AdminUser.create(email: 'pisandelli@gmail.com', password: 'pisandelli123')
