class Level < ActiveRecord::Base
  attr_accessible :title, :description, :image
  mount_uploader :image, DefaultImageUploader

  has_many :users
end
