# encoding: UTF-8
require 'spec_helper'

describe StoreDashboard::OffersController do
  it_behaves_like 'a store dashboard controller'

  context "store user is logged in" do
    login_store_user

    describe "index action" do
      it "should show all offers from current store" do
      end

    end

    describe "Coupons Management Page" do
      it "should show only the coupons from the current store user" do

      end
    end

    describe "Offers Management Page" do
      it "should show only the offers from the current store user" do

      end
    end

    describe "Statistics Page" do
      it "should Show all coupons" do

      end
    end

    describe "Profile Page" do

    end

    describe "Help Page" do

    end
  end
end
