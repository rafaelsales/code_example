module UserDashboardControllersSharedBehavior
  extend ActiveSupport::Concern

  included do
    before_filter :authenticate_user!
    before_filter :define_shared_variables

    layout 'user_dashboard'
  end

  private

  NUMBER_OF_TOP_RECORDS = 12

  def define_shared_variables
    @top_friends = current_user.friends.preview_info.order_by_last_sign_in.limit(NUMBER_OF_TOP_RECORDS)
    @top_badges = current_user.badges.preview_info.order_by_title.limit(NUMBER_OF_TOP_RECORDS)
  end
end
