module ControllerMacros
  def login_user
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in FactoryGirl.create(:user)
    end
  end

  def login_store_user
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:store_user]
      store_user = FactoryGirl.create(:store_user, :store => FactoryGirl.create(:store))
      sign_in store_user
    end
  end

  def login_admin_user
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:admin_user]
      sign_in FactoryGirl.create(:admin_user)
    end
  end
end
