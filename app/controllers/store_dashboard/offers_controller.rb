class StoreDashboard::OffersController < ApplicationController
  include StoreDashboardControllersSharedBehavior

  def index
    @offers = current_store.offers.order_by_most_recent
  end

  def edit
    @offer = Offer.find(params[:id])
  end

  def update
    @offer = Offer.find(params[:id])
    @offer.update_attributes(params[:offer])
    if @offer.errors.any?
      add_errors_for @offer
      render action: :edit
    else
      redirect_to action: :index unless errors?
    end
  end

  def new
    @offer = Offer.new
  end

  def create
    @offer = Offer.create(params[:offer].merge(store: current_store))
    if @offer.errors.any?
      add_errors_for @offer
      render action: :new
    else
      redirect_to action: :index unless errors?
    end
  end

  def show
    @offer = Offer.find(params[:id])
  end

  def activate
    @offer = Offer.find(params[:offer_id])
    @offer.activate
    redirect_to action: :index
  end

  def deactivate
    @offer = Offer.find(params[:offer_id])
    @offer.deactivate
    redirect_to action: :index
  end

  def destroy
    @offer = Offer.find(params[:id])
    @offer.destroy
    redirect_to action: :index
  end

end
