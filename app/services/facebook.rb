# Service class to access Facebook through Koala
# Usage: Facebook.for(user).friends
class Facebook

  class << self
    def for(user)
      self.new(user)
    end
  end

  def share_offer(offer)
    begin
      api.put_connections 'me', 'promrapp:get',
        'offer' => url_helper.offer_url(offer.id),
        'fb:explicitly_shared' => 'true'
      return true
    rescue => e
      Rails.logger.error "Unable to publish action on Facebook. Response: #{e.message}\Offer: #{offer.inspect}"
      return false
    end
  end

  def friends
    query = <<-FQL
      SELECT uid, name, profile_url, pic_square
      FROM user
      WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me())
      ORDER BY name
    FQL
    api.fql_query(query).map { |friend| OpenStruct.new(friend) }
  end

  def picture_url(options = { type: 'square' })
    # Valid keys and values for options:
    #   type: 'square', 'small', 'normal' and 'large'
    #   width: <value in pixels>
    #   height: <value in pixels>

    # api(@user).get_picture('me', options)
    self.class.build_url("#{@user.facebook_uid}/picture", options)
  end

  private

  def initialize user
    @user = user
  end

  def api
    @api ||= Koala::Facebook::API.new @user.facebook_oauth_token
  end

  def build_url(path, params = {})
    @server ||= Koala::HTTPService.server(use_ssl: true)
    query = params.empty? ? '' : "?#{params.to_query}"
    "#{@server}/#{path}#{query}"
  end

  def url_helper
    @url_helper ||= Rails.application.routes.url_helpers
  end
end
