class UserProfileController < ApplicationController
  include UserDashboardControllersSharedBehavior

  def show_me
    @user = current_user
    render :show
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user

    if @user.update_profile_attributes(params[:user])
      notices << 'Seus dados foram salvos com sucesso!'
      sign_in(@user, bypass: true)
    else
      add_errors_for @user
    end
    render action: :edit
  end

  def invite_friend
    @friend = User.find(params[:id])
    current_user.invite(@friend)
    notices << "Aguarde aprovacao do usuario #{@friend.name}."
    redirect_to(:back)
  end

  def cancel_invite
    @friend = User.find(params[:id])
    current_user.remove_friendship(@friend)
    notices << "A solicitacao de amizade com #{@friend.name} foi cancelada."
    redirect_to(:back)
  end

  def approve_friend
    @friend = User.find(params[:id])
    current_user.approve(@friend)
    notices << "Voce e #{@friend.name} agora sao amigos!"
    redirect_to(:back)
  end

  def remove_friend
    @friend = User.find(params[:id])
    current_user.remove_friendship(@friend)
    notices << "Voce e #{@friend.name} nao sao mais amigos."
    redirect_to(:back)
  end

  def block_friend
    @friend = User.find(params[:id])
    current_user.block(@friend)
    notices << "Voce bloqueou #{@friend.name}."
    redirect_to(:back)
  end

  def unblock_friend
    @friend = User.find(params[:id])
    current_user.unblock(@friend)
    notices << "Voce desbloqueou a amizade com #{@friend.name}."
    redirect_to(:back)
  end
end
