class Store < ActiveRecord::Base
  attr_accessible :name, :cnpj, :address, :phone, :contact, :image
  has_many :offers
  belongs_to :mall

  scope :preview_info, -> { select([:id, :name, :image]) }
  scope :order_by_name, -> { order('name') }

  usar_como_cnpj :cnpj
  mount_uploader :image, DefaultImageUploader
  acts_as_followable

  class << self
    def search(search)
      where('UPPER(name) LIKE UPPER(?)', "%#{search}%").preview_info.order_by_name
    end
  end

end
