# encoding: utf-8

class DefaultImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  # Include the Sprockets helpers for Rails 3.1+ asset pipeline compatibility:
  # include Sprockets::Helpers::RailsHelper
  # include Sprockets::Helpers::IsolatedHelper

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # process convert: 'jpg'

  # Create different versions of your uploaded files:
  version :small do
    process resize_to_fill: [50, 50]
  end

  version :medium do
    process resize_to_fill: [100, 100]
  end

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    group_dir = version_name ? version_name.to_s : 'original'
    "uploads/#{model.class.to_s.underscore}/#{formatted_model_id}"
  end

  def filename
    "#{mounted_as}.#{file.extension}"
  end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(jpg jpeg gif png)
  end

  private

  def formatted_model_id
    model.id.to_s.rjust(5, '0')
  end
end
