class CreateUbadges < ActiveRecord::Migration
  def change
    create_table :ubadges do |t|
      t.belongs_to :badge
      t.belongs_to :user
      t.timestamps
    end
  end
end
