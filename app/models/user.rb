class User < ActiveRecord::Base
  include Amistad::FriendModel
  attr_accessible :name, :username, :cpf, :email, :birthdate,
                  :image, :all_coupons_count, :used_coupons_count,
                  :password, :password_confirmation, :remember_me,
                  :facebook_uid, :facebook_oauth_token

  has_many :coupons
  has_many :unread_offer_shares, class_name: 'OfferShare', foreign_key: :to_user_id,
            conditions: { unread: true }, order: 'created_at DESC'
  has_many :offer_shares, order: 'created_at DESC', foreign_key: :to_user_id

  has_many :user_badges
  has_many :badges, through: :user_badges

  belongs_to :level

  validates_presence_of :name

  scope :preview_info, -> { select([:id, :name, :image]) }
  scope :order_by_last_sign_in, -> { order('last_sign_in_at DESC') }
  scope :order_by_name, -> { order('name') }

  mount_uploader :image, DefaultImageUploader
  usar_como_cpf :cpf
  acts_as_follower

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :token_authenticatable,
         :confirmable, :lockable, :omniauthable, :omniauth_providers => [:facebook]

  def allow_invite?(user)
    !invited?(user) && !friend_with_or_blocked?(user)
  end

  def allow_cancel_invite?(user)
    invited?(user) && !friend_with_or_blocked?(user)
  end

  def friend_with_or_blocked?(user)
    friend_with?(user) || blocked?(user)
  end

  def facebook_proxy
    @facebook_proxy ||= Facebook.for(self)
  end

  def should_review_profile?
    sign_in_count == 0
  end

  def update_profile_attributes new_attributes
    current_password = new_attributes.delete :current_password

    if current_password.present? && !valid_password?(current_password)
      errors.add(:base, 'Senha atual nao confere')
      return false
    elsif new_attributes[:password].blank? && new_attributes[:password_confirmation].blank?
      # Skip password update if the user has not provided the current password or the new password
      new_attributes.delete :password
      new_attributes.delete :password_confirmation
    end

    update_attributes(new_attributes, without_protection: true)
  end

  class << self
    def find_or_create_by_facebook!(auth, signed_in_resource = nil)
      User.find_or_initialize_by_facebook_uid(auth.uid).tap do |user|
        user.facebook_oauth_token = auth.credentials.token
        if user.new_record?
          user.assign_attributes(name: auth.info.name,
                                 email: auth.info.email,
                                 password: Devise.friendly_token[0, 6].downcase)
        end
        user.save!
      end
    end

    def map_by_facebook_uids facebook_users
      facebook_uids = facebook_users.map { |fb_user| fb_user.uid.to_s }
      User.where("facebook_uid IN (?)", facebook_uids)
          .inject({}) { |hash, user| hash.merge(user.facebook_uid => user) }
    end

    def search(search)
      where('UPPER(name) LIKE UPPER(?)', "%#{search}%").preview_info.order_by_name
    end
  end
end
