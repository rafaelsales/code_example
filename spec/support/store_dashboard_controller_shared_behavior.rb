shared_examples "a store dashboard controller" do
  context "store user not logged in" do
    context "a normal user is logged in" do
      login_user

      it "should redirect to store user sign in page" do
        get :index
        response.should redirect_to new_store_user_session_path
      end
    end

    context "an admin user is logged in" do
      login_admin_user

      it "should redirect to store user sign in page" do
        get :index
        response.should redirect_to new_store_user_session_path
      end
    end
  end

  context "store user is logged in" do
    login_store_user

    describe "index action" do
      it "should render the store_dashboard layout" do
        get :index
        response.should render_template("layouts/store_dashboard")
      end

      it "should render store dashboard page successfully" do
        get :index
        response.should be_success
      end
    end
  end
end
