class OffersController < ApplicationController
  include UserDashboardControllersSharedBehavior

  def show
    @offer = Offer.find params[:id]
  end

  def new_coupon
    offer = Offer.find params[:offer_id]
    offer.generate_coupon current_user

    notices = I18n.t 'offer.new_coupon_success'
    redirect_to action: :index, controller: :coupons
  end

end
