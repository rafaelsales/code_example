class AdminDashboard::StoreUsersController < ApplicationController
  include AdminDashboardControllersSharedBehavior

  def index
      @store_users = StoreUser.all
  end

  def create

  end

  def new
    @store_user = StoreUser.new
  end

  def show
    @store_user = StoreUser.find(params[:id])
    @store_user.update_attributes(params[:store_user])
    if @store_user.errors.any?
      add_errors_for @store_user
      render action: :edit
    else
      redirect_to action: :index unless errors?
    end
  end

end
