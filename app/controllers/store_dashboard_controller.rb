class StoreDashboardController < ApplicationController
  include StoreDashboardControllersSharedBehavior

  def index
  end

  def profile

  end

  def statistics

  end

  def help

  end

  def use_coupon
    code = params[:code].try(:upcase)
    @coupon = Coupon.find_by_code_and_store(code, current_store)

    if @coupon
      @coupon.use!
      add_errors_for(@coupon)
      notices << I18n.t('coupon.use_success', :code => code) unless errors?
    else
      errors << I18n.t('coupon.not_found', :code => code)
    end
    render action: :index
  end

  def change_image
    @store = current_store
    image = params[:image]
    @store.image = image
  end

end
