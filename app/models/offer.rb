class Offer < ActiveRecord::Base

  DURATION_LIMIT = 30

  class Status
    DEACTIVATED = 'deactivated'
    ACTIVATED = 'activated'
    ARCHIVED = 'archived'

    ALL = [DEACTIVATED, ACTIVATED, ARCHIVED]
  end

  attr_accessible :title, :description, :store, :status, :start_time, :end_time,
                  :coupons_provided, :prices_defined, :discount, :original_price, :current_price

  has_many :coupons
  belongs_to :store

  validates :title, :description, :start_time, :end_time, :coupons_provided, :discount, presence: true
  validates :original_price, :current_price, presence: true, if: ->(offer) { offer.prices_defined? }

  validates :coupons_provided, numericality: { only_integer: true }

  validate if: ->(o) { o.should_validate_dates } do |o|
    # Validates date range for end_time
    if o.end_time <= o.start_time
      o.errors.add :end_time, I18n.t('offer.validation.end_time')
    end
    # Validates offer exhibition duration
    if o.end_time.beginning_of_day > o.start_time.beginning_of_day + DURATION_LIMIT.days
      o.errors.add :end_time, I18n.t('offer.validation.duration_limit', limit: DURATION_LIMIT)
    end
  end

  scope :on_going do
    Offer.where('status <> ? AND end_time > ? AND coupons_count < coupons_provided',
          Status::ARCHIVED, Time.current)
  end
  scope :order_by_most_recent, -> { order('start_time DESC') }

  before_validation :before_validation
  before_destroy :before_destroy

  def can_delete?
    deactivated?
  end

  def can_edit?
    can_activate?
  end

  def finished?
    archived? || end_time < Time.current || coupons.size >= coupons_provided
  end

  def generate_coupon user
    coupons.build(user: user)
    save!
  end

  def on_going?
    started? && !finished?
  end

  def started?
    activated? && start_time < Time.current
  end

  def share from: nil, to: nil
    OfferShare.create(from_user: from, to_user: to, offer: self)
  end

  state_machine :status, initial: :deactivated do
    transition :deactivated => :activated, on: :activate, if: ->(offer) do
      !offer.finished?
    end

    transition :activated => :deactivated, on: :deactivate, if: ->(offer) do
      offer.coupons.empty? && !offer.finished?
    end

    transition any => :archived, on: :archive
  end

  class << self
    def search(search)
      where('UPPER(title) LIKE UPPER(?)', "%#{search}%")
    end
  end

  protected

  def should_validate_dates
    !archived? && end_time.present? && start_time.present? && (end_time_changed? || start_time_changed?)
  end

  private

  def before_destroy
    return true if can_delete?
    errors.add(:base, 'Para excluir a oferta deve estar desativada.')
    false
  end

  def before_validation
    if prices_defined? && original_price && current_price
      # Calculates the discount based on the prices filled in the form
      self.discount = BigDecimal(100) - (BigDecimal(100) / original_price * current_price).floor
    elsif !prices_defined?
      self.original_price = self.current_price = nil
    end
  end
end
