class OfferPresenter
  attr_accessor :offer

  def initialize offer
    self.offer = offer
  end

  def offer_status
    if offer.activated?
      if offer.on_going?
        I18n.t "offer.status.activated_visible"
      elsif !offer.started?
        remaining_time = 1 + Time.diff(offer.start_time, Time.current, '%h')[:diff].to_i
        I18n.t "offer.status.activated_pending", hours: remaining_time
      else
        I18n.t "offer.status.activated_finished"
      end
    else
      I18n.t "offer.status.#{offer.status}"
    end
  end

  def offer_coupon_status
    "#{offer.coupons.size} / #{offer.coupons_provided}"
  end
end
