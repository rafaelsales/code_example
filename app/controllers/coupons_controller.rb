class CouponsController < ApplicationController
  include UserDashboardControllersSharedBehavior

  def index
    @coupons = current_user.coupons
  end

  def show
    @coupon = Coupon.find_by_id_and_user_id params[:id], current_user
    redirect_to action: :index unless @coupon
  end
end
