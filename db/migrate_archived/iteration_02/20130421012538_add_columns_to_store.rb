class AddColumnsToStore < ActiveRecord::Migration
  def change
    change_table :stores do |t|
      t.integer :available_coupons
    end
  end
end
