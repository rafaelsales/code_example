class CreateCouponPurchases < ActiveRecord::Migration
  def change
    create_table :coupon_purchases do |t|
      t.boolean :processed, :default => false
      t.integer :amount
      t.belongs_to :store
      t.timestamps
    end
  end
end
