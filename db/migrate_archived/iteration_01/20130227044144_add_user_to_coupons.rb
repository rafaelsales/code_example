class AddUserToCoupons < ActiveRecord::Migration
  def change
    change_table(:coupons) do |t|
      t.belongs_to :user #Could also be t.references
    end
    add_index :coupons, :user_id
  end
end
