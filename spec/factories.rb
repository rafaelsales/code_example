FactoryGirl.define do

  factory :mall do
    name { Faker::Name.name }
  end

  factory :store do
    ignore do
      image_name { ['subway.jpg', 'skyler.jpg', 'sushiloko.jpg'].sample }
    end

    name { Faker::Name.name }
    mall { build(:mall) }
    address { "#{Faker::Address.street_name}, #{Faker::Address.building_number}\n
               #{Faker::Address.secondary_address}\n
               #{Faker::Address.city} - #{Faker::Address.state_abbr}\n
               #{Faker::Address.zip_code}" }
    phone { '(85) ' + Random.rand(999).to_s.rjust(4, '9') + '-' +  Random.rand(9999).to_s.rjust(4, '0')}
    contact { Faker::Name.name }
    image { sample_image('store', image_name) }

    factory :store_with_offers do
      ignore do
        offers_count 2
      end

      after(:build) do |store, evaluator|
        evaluator.offers_count.times { build :offer, store: store }
      end
    end
  end

  factory :offer do
    title { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraphs(5).join }
    store { build :store }
    start_time 1.day.ago
    end_time 3.days.from_now
    coupons_provided 10
    prices_defined { [true, false].sample }
    discount { (Random.rand(3) + 4) * 10 }
    original_price { Random.rand(50) + 100 if prices_defined? }
    current_price { original_price / BigDecimal(100) * discount if prices_defined? }
    status Offer::Status::ACTIVATED

    after(:build) do |offer, evaluator|
      offer.coupons.each { |coupon| coupon.offer = offer }
    end

    trait :archived do
      status Offer::Status::ARCHIVED
      start_time 4.days.ago
      end_time 1.day.ago
    end

    factory :archived_offer, traits: [:archived]
  end

  factory :coupon do
    offer { build :offer }
    user { build :user }
  end

  factory :offer_share do
    offer { build :offer }
    from_user { build :user }
    to_user { build :user }
    unread true
  end

  factory :user do
    ignore do
      image_name { "#{(1..7).to_a.sample}.jpg" }
    end

    name { Faker::Name.name }
    email { Faker::Internet.email }
    birthdate { random_date }
    password 'password' #this will be encrypted
    confirmed_at Time.current
    image { sample_image('user', image_name) }
  end

  factory :store_user do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    password 'password' #this will be encrypted
  end

  factory :admin_user do
    email { Faker::Internet.email }
    password 'password' #this will be encrypted
  end

  factory :badge do
    ignore do
      image_name { "#{(1..7).to_a.sample}.png" }
    end

    title { Faker::Name.name }
    description { Faker::Lorem.sentence }
    image { sample_image('badge', image_name) }
  end
end

def sample_image(resource, file_name)
  Rails.root.join('public', 'sample_images', resource, file_name).open
end

def random_date
  Date.new((Date.today.year - 30..Date.today.year - 20).to_a.sample, (1..12).to_a.sample, (1..28).to_a.sample)
end
