namespace :db do
  desc "Load site sample data for development environment"
  task :load_sample_data => ['db:schema:load', 'db:seed'] do
    raise 'only allowed on development and test environments' if false && !(Rails.env == 'development' || Rails.env =~ /test$/)
    # Remove uploaded files so that it doesn't mess up with new ones
    FileUtils.rm_rf(Dir.glob(Rails.root.join('public', 'uploads', '*')))

    puts "\nLoading sample data"
    require File.join(Rails.root, 'db', 'sample_data')
    puts "Done loading sample data"
  end
end
