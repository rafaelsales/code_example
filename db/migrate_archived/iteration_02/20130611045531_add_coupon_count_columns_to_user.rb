class AddCouponCountColumnsToUser < ActiveRecord::Migration
  def change
    add_column :users, :all_coupons_count, :integer
    add_column :users, :used_coupons_count, :integer
  end
end
