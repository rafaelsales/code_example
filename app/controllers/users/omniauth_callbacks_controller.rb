class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def facebook
    oauth_response = request.env["omniauth.auth"] # All information retrieved from the provider by OmniAuth
    user = User.find_or_create_by_facebook!(oauth_response, current_user)

    session["devise.facebook_data"] = oauth_response

    if user.should_review_profile?
      sign_in user, :event => :authentication #this will throw if @user is not activated
      redirect_to edit_user_profile_path
    else
      sign_in_and_redirect user, :event => :authentication #this will throw if @user is not activated
    end
  end
end
