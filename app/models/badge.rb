class Badge < ActiveRecord::Base
  attr_accessible :title, :description, :image
  mount_uploader :image, DefaultImageUploader

  scope :preview_info, -> { select([:id, :title, :image]) }
  scope :order_by_title, -> { order('title') }
end
