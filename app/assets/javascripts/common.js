var Promr = Promr || (function() {
  var language = 'pt-BR';
  var bootstrapDateTimeFormat = {
    format: 'dd/MM/yyyy hh:mm',
    language: 'pt-BR'
  };
  var bootstrapDateFormat = {
    format: 'dd/MM/yyyy',
    language: 'pt-BR',
    pickTime: false
  };

  return {
    BOOTSTRAP_DATE_PICKER_FORMAT: bootstrapDateFormat,
    BOOTSTRAP_DATE_TIME_PICKER_FORMAT: bootstrapDateTimeFormat,
    LANGUAGE: language
  };
})();

jQuery.fn.datetimepicker.defaults['language'] = Promr.LANGUAGE;
