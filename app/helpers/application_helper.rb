module ApplicationHelper
  def facebook_meta_tags attributes
    attributes = attributes.merge "fb:app_id" => Rails.application.config.facebook.app_id,
                                  "og:locale" => Rails.application.config.facebook.locale
    tags = attributes.map do |property, content|
      tag(:meta, property: property, content: content)
    end
    tags.join.html_safe
  end

  def label_for model_and_attribute
    I18n.t "helpers.label.#{model_and_attribute}"
  end

  def currency(number)
    number_to_currency(number)
  end

  def percentage(number)
    number_to_percentage(number, precision: 0)
  end
end
