class ApplicationController < ActionController::Base
  protect_from_forgery

  layout :layout_by_resource

  before_filter :define_default_route_options

  def define_default_route_options
    Rails.application.routes.default_url_options.merge!(host: request.host, port: request.port, protocol: request.scheme)
  end

  def notices
    flash[:notices] ||= []
  end

  def errors
    flash[:errors] ||= []
  end

  def errors?
    flash[:errors].present?
  end

  def add_errors_for model
    errors.concat model.errors.full_messages if model.errors.any?
  end

  protected

  def layout_by_resource
    if devise_controller?
      if resource_name == :store_user || resource_name == :admin_user
        "dashboard_login"
      else resource_name == :user
        "home"
      end
    end
  end
end
