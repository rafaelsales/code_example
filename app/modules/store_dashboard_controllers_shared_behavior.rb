module StoreDashboardControllersSharedBehavior
  extend ActiveSupport::Concern

  included do
    before_filter :authenticate_store_user!

    layout 'store_dashboard'
    helper :store_dashboard
  end

  private
  def current_store
    current_store_user.store
  end
end
