class RenameUserBadgesTable < ActiveRecord::Migration
  def change
    rename_table :ubadges, :user_badges
  end
end
