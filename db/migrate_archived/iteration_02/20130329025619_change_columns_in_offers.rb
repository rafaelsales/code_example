class ChangeColumnsInOffers < ActiveRecord::Migration
  def change
    remove_column :offers, :coupons_count
    remove_column :offers, :expires_at

    change_column :offers, :description, :text

    add_column :offers, :status, :string
    add_column :offers, :start_time, :timestamp
    add_column :offers, :end_time, :timestamp
    add_column :offers, :coupons_provided, :integer
    add_column :offers, :coupon_expires_on, :date
    add_column :offers, :prices_defined, :boolean
    add_column :offers, :discount, :integer
    add_column :offers, :original_price, :decimal, precision: 6, scale: 2
    add_column :offers, :current_price, :decimal, precision: 6, scale: 2
    add_column :offers, :coupons_count, :integer

    add_index :offers, :status
    add_index :offers, :start_time
    add_index :offers, :end_time
  end
end
