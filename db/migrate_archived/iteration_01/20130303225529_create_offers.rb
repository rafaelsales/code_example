class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.string :title
      t.string :description
      t.timestamp :expires_at
      t.integer :coupons_count, default: 0
      t.belongs_to :store
      t.timestamps
    end

    add_index :offers, :store_id
  end
end
