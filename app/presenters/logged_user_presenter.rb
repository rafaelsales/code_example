class LoggedUserPresenter
  def initialize user
    @user = user
  end

  def has_unread_offer_shares?
    unread_offer_shares_count > 0
  end

  def has_unread_notifications?
    unread_notifications_count > 0
  end

  def unread_offer_shares_count
    @user.unread_offer_shares.size
  end

  def unread_notifications_count
    0
  end

end
