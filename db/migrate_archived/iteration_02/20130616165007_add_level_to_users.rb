class AddLevelToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.belongs_to :level
    end
  end
end
