require 'spec_helper'

describe Coupon do

  it { should belong_to :offer }
  it { should belong_to :user }

  describe 'before_create' do
    it 'should assign code' do
      coupon = build :coupon
      coupon.expects(:code=)
      coupon.save
    end

    it 'generate another code if the first generated already exists' do
      first_code = 'A1B2C3'
      Coupon.any_instance.expects(:generate_code).returns(first_code)
      create :coupon

      second_code = 'B1C2D3'
      Coupon.any_instance.expects(:generate_code)
        .twice.returns(first_code, second_code)
      expect(create(:coupon).code).to be second_code
    end
  end

  describe 'can_be_used?' do
    it 'returns true if coupon was not used and offer is still on going' do
      coupon = build(:coupon)
      expect(coupon.can_be_used?).to be true
    end

    it 'returns false if coupon was used' do
      coupon = build(:coupon, used_at: Time.now)
      expect(coupon.can_be_used?).to be false
    end

    it 'returns false if offer has finished' do
      coupon = create(:coupon, offer: create(:archived_offer))
      expect(coupon.can_be_used?).to be false
    end
  end

  describe 'use!' do
    context 'when it can be used' do
      before do
        @coupon = build :coupon
      end

      it 'returns true' do
        expect(@coupon.use!).to be true
      end

      it 'assign used_at with current time' do
        Timecop.freeze do
          @coupon.use!
          expect(@coupon.used_at).to eq Time.now
        end
      end

      it "increases the user's number of used coupons" do
        expect(@coupon.user.used_coupons_count).to be 0
        @coupon.use!
        expect(@coupon.user.used_coupons_count).to be 1
      end
    end

    context 'when it cannot be used' do
      it 'adds error and returns false if coupon was used' do
        coupon = create :coupon
        coupon.use!
        expect(coupon.use!).to be false
        expect(coupon.errors.count).to be 1
      end

      it 'adds error and returns false if offer has finished' do
        coupon = create :coupon, offer: create(:archived_offer)
        expect(coupon.use!).to be false
        expect(coupon.errors.count).to be 1
      end
    end
  end

  describe 'used?' do
    it 'returns true when used_at is present' do
      coupon = build(:coupon, used_at: Time.now)
      expect(coupon.used?).to be true
    end

    it 'returns false when used_at is present' do
      coupon = build(:coupon, used_at: nil)
      expect(coupon.used?).to be false
    end
  end

  describe 'self.find_by_id_and_user_id' do
    it 'returns only the coupon that matches' do
      coupon = create :coupon
      unexpected_coupon = create :coupon

      result = Coupon.find_by_id_and_user_id(coupon.id, coupon.user)
      expect(result).to eq coupon
    end

    it 'returns nil if someone tries to hack a coupon' do
      hacker = create :user
      coupon = create :coupon

      result = Coupon.find_by_id_and_user_id(coupon.id, hacker)
      expect(result).to be nil
    end
  end

  describe 'self.find_by_code_and_store' do
    it 'returns only the coupon that matches' do
      coupon = create :coupon
      unexpected_coupon = create :coupon

      result = Coupon.find_by_code_and_store(coupon.code, coupon.offer.store)
      expect(result).to eq coupon
    end

    it 'returns nil if a coupon does not belong a store' do
      another_store = create :store
      coupon = create :coupon

      result = Coupon.find_by_code_and_store(coupon.code, another_store)
      expect(result).to be nil
    end
  end
end
