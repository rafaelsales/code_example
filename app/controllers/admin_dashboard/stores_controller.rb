class AdminDashboard::StoresController < ApplicationController
  include AdminDashboardControllersSharedBehavior

  def index
    @stores = Store.all
  end

  def create

  end

  def show
    @store = Store.find(params[:id])
  end

  def edit
    @store = Store.find(params[:id])
  end

  def update
    @store = Store.find(params[:id])
    @store.update_attributes(params[:store])
    if @store.errors.any?
      add_errors_for @store
      render action: :edit
    else
      redirect_to action: :index unless errors?
    end
  end

  def provide_coupons
  end

end
