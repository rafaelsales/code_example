class HomeController < ApplicationController
  layout 'home'

  def index
  end

  def contact_us
  end

  def terms
  end
end
